# [FR] Traduction française pour _Nautilus Open Any Terminal_.
# [EN] French traduction for _Nautilus Open Any Terminal_.

_French .po file for [https://github.com/Stunkymonkey/nautilus-open-any-terminal](https://github.com/Stunkymonkey/nautilus-open-any-terminal)._

![README](assets/img/README.png)

## Procedure.
Based on [the original .pot template](https://github.com/Stunkymonkey/nautilus-open-any-terminal/blob/master/nautilus_open_any_terminal/locale/template.pot) and translated with Poedit.