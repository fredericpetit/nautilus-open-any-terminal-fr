# VERBATIM.

`Model : 2023-04-13.`

## License.
1. choose license from [https://spdx.org/licenses/](https://spdx.org/licenses/).

## Tokens.
- no need.

## CI/CD.
- no need.

## Composer.
- no need.

## Pipeline.
- no need.

## Rights.
- adapt some visibility rights (web access releases for example).